#include <iostream>
#include "track.h"


Track::Track() :
  id(0),
  duration(0),
  name(""),
  path(""),
  an_artist(),
  an_album(),
  a_polyphony(),
  a_format(),
  a_subgenre(),
  a_genre()
{
  std::cout << "Constructeur par défaut de Track" << std::endl;
}

Track::Track(unsigned int _id, std::chrono::duration<int> _duration, std::string _name, std::string _path, Artist _an_artist, Album _an_album, Polyphony _a_polyphony, Format _a_format, SubGenre _a_subgenre, Genre _a_genre) :
  id(_id),
  duration(_duration),
  name(_name),
  path(_path),
  an_artist(_an_artist),
  an_album(_an_album),
  an_polyphony(_a_polyphony),
  an_format(_a_format),
  an_subgenre(_a_subgenre),
  an_genre(_a_genre)
{
  std::cout << "Constructeur avec paramètres pour 1 artiste et 1 album de Track" << std::endl;
}


unsigned int Track::get_id()
{
  return id;
}

std::chrono::duration<int> Track::get_duration()
{
  return duration;
}

std::string Track::get_name()
{
  return name;
}

std::string Track::get_path()
{
  return path;
}

