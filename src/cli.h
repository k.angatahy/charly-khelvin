#ifndef CLI_H
#define CLI_H
#include <gflags/gflags.h>
#include <gflags/gflags_declare.h>


DECLARE_int64(duration);
DECLARE_string(genre);
DECLARE_string(type);
DECLARE_string(subgenre);
DECLARE_string(name);


#endif
