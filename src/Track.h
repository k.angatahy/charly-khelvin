#ifndef TRACK_H
#define TRACK_H

#define TRACK_H
#include <iostream>
#include <string>
#include <vector>
#include <chrono>
#include "artist.h"
#include "album.h"
#include "polyphony.h"
#include "format.h"
#include "subgenre.h"
#include "genre.h"

class Track

{
 public:
  Track();
  ~Track();

  Track(unsigned int, int, std::string, std::string, Artist, Album, Polyphony, Format, Subgenre, Genre);

  std::chrono::duration<int> get_duration();

  std::string get_name();


  std::string get_path();


  unsigned int get_id();


  Artist an_artist;

  Album an_album;

  Polyphony an_polyphony;

  Format an_format;

  Subgenre an_subgenre;

  Genre an_genre;


 private:

  unsigned int id;

  std::chrono::duration<int> duration;

  std::string name;

  std::string path;

};
#endif
