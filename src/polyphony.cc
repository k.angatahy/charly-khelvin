#include "polyphony.h"

#include <iostream>
#include <string>

Polyphony::Polyphony() :
  id(0),
  number(0)
{}

Polyphony::Polyphony(unsigned int _id, short int _number) :
  id(_id),
  number(_number)
{}

Polyphony::~Polyphony()
{
  std::cout << "Destructeur par défaut" << std::endl;
}

unsigned int Polyphony::get_id()
{
  return id;
}

void Polyphony::set_id(unsigned int _id)
{
  id = _id;
}

short int Polyphony::get_number()
{
  return number;
}

void Polyphony::set_number(short int _number)
{
  number = _number;
}
