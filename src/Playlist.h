#include <iostream>


class Playlist
{
 public:
  Playlist();
  ~Playlist();

  int getId();
  void setId(unsigned int _id);

  int getDuration();
  void setDuration(int _duration);

  void writeXSPF();
  void writeM3U();

 private:
  int id;
  int duration;
};
