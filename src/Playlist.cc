#include "Playlist.h"

#include <iostream>
#include <string>

#include <libpq-fe.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <string.h>
#include <stdlib.h>
#include <cstring>

#include <XspfWriter.h>
#include <XspfTrack.h>
#include <XspfIndentFormatter.h>

Playlist::Playlist() :
  id(0),
  duration(0)
{}

int Playlist::getId()
{
  return id;
}

void Playlist::setId(unsigned int _id)
{
  id = _id;
}

int Playlist::getDuration()
{
  return duration;
}

void Playlist::setDuration(int _duration)
{
  duration = _duration;
}

void Playlist::writeXSPF()
{
  int code_retour;
  const char requete_sql[] = "SET SCHEMA 'radio_libre'; SELECT * FROM \"morceau\" inner join \"artiste_morceau\" on \"morceau\".id = \"artiste_morceau\".id_morceau inner join \"artiste\" on \"artiste_morceau\".id_artiste = \"artiste\".id;";
  char informations_de_connexion[] = "host=postgresql.bts-malraux72.net port=5432 dbname=Cours user=c.jousse password=P@ssword connect_timeout=4";
  PGPing code_retour_ping;
  PGconn *connexion(nullptr);
  PGresult *resultat_requete(nullptr);
  ExecStatusType etat_resultat;
  ConnStatusType code_retour_connexion;
  char *reponse_requete(nullptr);
  //fin de la connexion
  int nbLignes;
  int nbColonnes;
  int l, c;
  char *nom_colonne(nullptr);
  char *nom_morceau(nullptr);
  char *nom_artiste(nullptr);
  connexion = PQconnectdb(informations_de_connexion);
  resultat_requete = PQexec(connexion, requete_sql);
  
  nbLignes = PQntuples(resultat_requete);
  nbColonnes = PQnfields(resultat_requete);
  //test
  int longueurTitre = 0;
  XML_Char const *const baseUri = _PT("http://radio6mic.net/");
  Xspf::XspfIndentFormatter formatter;
  int *xspf_error_code = 0;
  Xspf::XspfWriter *const writer = Xspf::XspfWriter::makeWriter(formatter, baseUri);
  Xspf::XspfTrack *track;

  for(l = 0; l < nbLignes - 1; l++)
  {
    nom_morceau = PQgetvalue(resultat_requete, l, 2);
    nom_artiste = PQgetvalue(resultat_requete, l, 11);
    track = new Xspf::XspfTrack();
    //ecriture xspf

    //ajout des titres
    track->lendCreator(_PT(nom_artiste));
    track->lendTitle(_PT(nom_morceau));

    writer->addTrack(track);
    // fin de l'ecriture xspf
  }

  writer->writeFile(_PT("TEST.xspf"));

 
  for(c = 0; c < nbColonnes; c++)
  {
    reponse_requete = PQgetvalue(resultat_requete, l, c);
  }
}




void Playlist::writeM3U()
{
  int code_retour;
  const char requete_sql[] = "SET SCHEMA 'radio_libre'; SELECT * FROM \"morceau\" inner join \"artiste_morceau\" on \"morceau\".id = \"artiste_morceau\".id_morceau inner join \"artiste\" on \"artiste_morceau\".id_artiste = \"artiste\".id;";
  char informations_de_connexion[] = "host=postgresql.bts-malraux72.net port=5432 dbname=Cours user=c.jousse password=P@ssword connect_timeout=4";
  PGPing code_retour_ping;
  PGconn *connexion;
  PGresult *resultat_requete;
  ExecStatusType etat_resultat;
  ConnStatusType code_retour_connexion;
  char *reponse_requete;
  //fin de la connexion
  int nbLignes;
  int nbColonnes;
  int l, c;
  char *nom_colonne;
  char *nom_morceau;
  char *nom_artiste;
  char *chemin;
  // reponse_requete =  PQgetvalue(resultat_requete, 2, 2);//Le programme va aller chercher la cellule en 2-2 de la reponse de la requete envoyée.

  connexion = PQconnectdb(informations_de_connexion);                                                                                    
  resultat_requete = PQexec(connexion, requete_sql);
 
  nbLignes = PQntuples(resultat_requete);
  nbColonnes = PQnfields(resultat_requete);
  //test
  int longueurTitre = 0;
  XML_Char const *const baseUri = _PT("http://radio6mic.net/");
  Xspf::XspfIndentFormatter formatter;
  int *xspf_error_code = 0;
  std::ofstream tmp;
  tmp.open("test.m3u");

  for(l = 0; l < nbLignes - 1; l++)
  {
    chemin = PQgetvalue(resultat_requete, l, 3);
    tmp << chemin << std::endl;
    // fin de l'ecriture m3u
  }

  tmp.close();

  for(c = 0; c < nbColonnes; c++)
  {
    reponse_requete = PQgetvalue(resultat_requete, l, c);
  }
}
