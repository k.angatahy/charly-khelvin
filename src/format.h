#ifndef FORMAT_H
#define FORMAT_H

#include <iostream>
#include <string>

class Format
{
 public:
  Format();
  ~Format();
  Format(unsigned int, std::string);
  unsigned int get_id();
  void set_id(unsigned int);
  std::string get_titule();
  void set_titule(std::string);

 private :
  unsigned int id;
  std::string titule;
};

#endif
